const test = require('ava');
const compose = require('..');
console.log(compose);

test('it calls the first function', function(t) {
  return compose((next, a, b) => {
    t.is(a, 1);
    t.is(b, 2);
  })(1, 2);
});

test('it provides a next argument to call the subsequent functions', function(t) {
  return compose(
    next => next(),
    next => next(),
    (next, a, b) => {
      t.is(a, 1);
      t.is(b, 2);
    }
  )(1, 2);
});

test('it allows overriding arguments', function(t) {
  return compose(
    next => next(),
    (next, a, b) => {
      t.is(a, 1);
      t.is(b, 2);
      return next(2, 3);
    },
    (next, a, b) => {
      t.is(a, 2);
      t.is(b, 3);
    },
    (next, a, b) => {
      t.is(a, 2);
      t.is(b, 3);
    }
  )(1, 2);
});

test('the composed function returns a resolved promise if no arguments are passed', t => {
  return compose()().then(t.pass());
});

test('the composed function returns a promise resolving to value if the only argument is not a function', t => {
  return compose(5)().then(a => t.is(a, 5));
});

test('the composed function returns a promise resolving to the value of the function', t => {
  return compose(() => 5)().then(a => t.is(a, 5));
});

test('the composed function catches error thrown by the composed function', t => {
  return compose(() => {
    throw new Error('boom!');
  })().catch(() => t.pass());
});
