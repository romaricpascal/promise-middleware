module.exports = function compose(...fns) {
  return async function(...args) {
    return nextify(fns, 0)();

    function nextify(fns, index) {
      return async function(...newArgs) {
        if (typeof fns[index] == 'function') {
          return fns[index](
            nextify(fns, index + 1),
            ...(newArgs.length ? newArgs : args)
          );
        } else {
          return fns[index];
        }
      };
    }
  };
};
