promise-middleware
---

Compose functions with more control over the flow of execution than with a simple `pipe()`. Each of the composed function receives a `next` callback to decide when to invoke the next function, if at all.

- Results are chained through promises allowing events to happen asynchronously
- Middleware functions have the following signature: `fn(next,...args)`
- `next()` allows to update the arguments mid-chain
